<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(BlackBeltsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(DanListsTableSeeder::class);
        $this->call(RoleTableSeeder::class);
    }
}
