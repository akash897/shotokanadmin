<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'name' => 'camp 2015',
        ]);

        DB::table('events')->insert([
            'name' => 'camp 2016',
        ]);

        DB::table('events')->insert([
            'name' => 'camp 2017',
        ]);
    }
}
