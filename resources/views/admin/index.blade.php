
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="./img/apple-icon.png">
    <link rel="icon" type="image/png" href="./img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Now UI Dashboard by Creative Tim
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/now-ui-dashboard.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="./demo/demo.css" rel="stylesheet" />
</head>
<style>
    .form-control {
        border: 0;
    }
</style>
<body class="">
<div class="wrapper ">
    @include('layouts.side-nav')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.nav')
        <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">Profile</h5>
                        </div>
                        <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5 pr-1">
                                        <div class="form-group">
                                            <label>Name</label>
{{--                                            <input type="label" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">--}}
                                            <lable class="form-control">{{ $user->name ? $user->name : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    <div class="col-md-3 px-1">
                                        <div class="form-group">
                                            <label>Role</label>
                                            <lable class="form-control">{{ $user->role->name ? $user->role->name : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <lable class="form-control">{{ $user->email ? $user->email : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <lable class="form-control">{{ $user->name ? $user->name : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Phone No.</label>
                                            <lable class="form-control">{{ $user->phone_no ? $user->phone_no : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group">
                                            <label>Created By</label>
                                            <lable class="form-control">{{ $user->created_by ? $user->created_by : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <div class="form-group">
                                            <label>Modified By</label>
                                            <lable class="form-control">{{ $user->modified_by ? $user->modified_by : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label>Account Status</label>
                                            <lable class="form-control label label-success">{{ $user->deleted_at ? 'Deactivated' : 'Active' }}</lable>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pr-1">
                                        <div class="form-group">
                                            <label>Created At</label>
                                            <lable class="form-control">{{ $user->created_at ? $user->created_at : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    <div class="col-md-4 px-1">
                                        <div class="form-group">
                                            <label>Updated At</label>
                                            <lable class="form-control">{{ $user->updated_at ? $user->updated_at : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                    @if($user->deleted_at != null)
                                        <div class="col-md-4 pl-1">
                                            <div class="form-group">
                                                <label>Deleted At</label>
                                                <lable class="form-control">{{ $user->deleted_at ? $user->deleted_at : 'N/A' }}</lable>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="./img/bg5.jpg" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="./img/mike.jpg" alt="...">
                                    <h5 class="title">{{Auth::user()->name}}</h5>
                                </a>
                                <p class="description">
                                    {{Auth::user()->email}}
                                </p>
                            </div>
                            <p class="description text-center">
                                {{Auth::user()->role->name}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
<!--   Core JS Files   -->
<script src="./js/core/jquery.min.js"></script>
<script src="./js/core/popper.min.js"></script>
<script src="./js/core/bootstrap.min.js"></script>
<script src="./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./js/now-ui-dashboard.min.js?v=1.1.0" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="./demo/demo.js"></script>
</body>

</html>