<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoogleApiKey extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];


}
