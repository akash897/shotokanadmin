<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Route::get('/login',function (){
    return view('admin.login');
})->name('login');
Route::post('/login', 'UserController@login')->name('auth.login');
Route::get('/password/reset', 'UserController@reset')->name('password.request');
Route::post('/logout', 'UserController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'middleware' => 'auth'], function()
{
    Route::get('/', 'UserController@dashboard')->name('dashboard');


    Route::get('/profile', 'UserController@profile')->name('profile');


    Route::get('/user', 'UserController@index')->name('user.index');


});